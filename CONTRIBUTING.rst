Contributing
============

Issues
------

Have a problem? Feel free to open an issue. Please search the issues before
opening a new one, your concern may already be noted (or wontfixed, depending
on the nature of your concern).

Pull Requests
-------------

The more people polish, clean, and improve these requirements the better
they'll be. So by all means, if you have an improvement, submit a PR and it'll
get responded to in a timely manner.

That said, we understand we'll probably have to reject many PRs from people who
want to improve 'hireability' at the cost of normalizing bad behavior (which is
somewhat the antithesis of this entire endeavour). We'll do our best to explain
why this is the case, but we hope that you won't view this as a personal
attack.  By nature, this project is exceedingly opinionated.