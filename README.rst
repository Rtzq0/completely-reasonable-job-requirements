The Completely Reasonable Job Requirements
==========================================

The CRJR is a project designed to make it easy for people to feel confident
in and and safe in seeking positive, balanced negotiation between themselves
and recruiters by broadcasting a baseline tone. Too frequently job-seekers
are put on the defensive by being forced to justify why they want crazy things
like diversity, flex time, vacations, or reasonable limits around on-call.

The CRJR is an effort to help simplify this process. Here's how you do it:

1. Say "If you're interested in having me join your team, project, partnership,
   etc. please see my requirements."
2. Link to https://rtzq0.gitlab.io/completely-reasonable-job-requirements/

   a. Optionally use the completely reasonable badge at:
   https://rtzq0.gitlab.io/completely-reasonable-job-requirements/badge.html

Credits
-------

- Sarah Jamie Lewis (@SarahJamieLewis): original letter
- Jason Ritzke (@Rtzq0): slight rewording, code
